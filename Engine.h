//
// Created by Roman Vozka on 18.09.2022.
//

#ifndef GESTURES_RECOGNIZER_ENGINE_H
#define GESTURES_RECOGNIZER_ENGINE_H

#include <stdio.h>
#include <opencv2/videoio.hpp>
#include <opencv2/imgproc.hpp>
#include "opencv2/highgui.hpp"

class Engine {
public:
    Engine();
    void run();

private:
    cv::VideoCapture videoCapture;
    cv::Mat frame;
    cv::Size windowSize;

    void captureCameraFrame();
};


#endif //GESTURES_RECOGNIZER_ENGINE_H
