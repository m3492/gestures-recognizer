# Gestures Recognizer

Project has been developed using Microsoft Visual Studio 2022 C++, OpenCV 4.6.0 and CMake.

Download and install OpenCV 4.6.0 from https://opencv.org/releases

Add ${OpenCV_HOME}\build\x64\vc15\bin and ${OpenCV_HOME}\build\x64\vc15\lib to Path

Create _OpenCV_DIR_ variable with value ${OpenCV HOME}\build\x64\vc15\lib

