//
// Created by hladk on 18.09.2022.
//

#include "Engine.h"

Engine::Engine(){
    videoCapture.open("C:\\Projects\\gitlab\\mendelu\\gestures-recognizer\\videos\\simple.mp4");
    windowSize = cv::Size(640, 480);
}

void Engine::run(){
    for (;;) {
        captureCameraFrame();

        cv::imshow("Video Stream", frame);

        int delay = 100; // ms, increase this to slow down the video processing and preview
        char c = cv::waitKey(delay);
        if (c == 27) {
            break;
        }
    }
}

void Engine::captureCameraFrame(){
    videoCapture >> frame;

    // loop the video, so it starts over if it reaches its end
    if (frame.empty()) {
        videoCapture.set(cv::CAP_PROP_POS_FRAMES, 0);
        videoCapture >> frame;
    }

    // initial processing of the input stream
    cv::resize(frame, frame, windowSize);
}